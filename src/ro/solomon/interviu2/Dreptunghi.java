package ro.solomon.interviu2;

public class Dreptunghi extends Forma {
	
private double lungime;
private double latime;


public Dreptunghi(double lg, double lat) {
this(lg, lat, "Dreptunghi");
}


Dreptunghi(double lg, double lat, String nume) {
super(nume);
lungime = lg;
latime = lat;
}
@Override
public double perimetrulCircumferinta(){
	return  2*(latime+lungime);
}
@Override
public double arie() {
return lungime * latime;
}
}


