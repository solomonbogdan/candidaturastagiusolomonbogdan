package ro.solomon.interviu2;

public class Cerc extends Forma {
	
private double raza;

public Cerc(double rad) {
super("Cerc");
raza = rad;
}

public double perimetrulCircumferinta(){
	return  Math.PI * raza * 2;
}
@Override
public double arie() {
 return Math.PI * Math.pow(raza, 2);
  }
}

