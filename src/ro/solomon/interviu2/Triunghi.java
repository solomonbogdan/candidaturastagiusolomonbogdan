package ro.solomon.interviu2;



public class Triunghi extends Forma{
	
	
	private double lat1;
	private double lat2;
	private double lat3;
	 


	public Triunghi(double x, double y, double z) {
	this(x, y, z, "Triunghi");
	}


	Triunghi(double x, double y, double z, String nume) {
	super(nume);
	lat1 = x;
	lat2 = y;
	lat3 = z;
	}
    @Override
	public double perimetrulCircumferinta(){
		return  lat1+lat2+lat3;
	}
    @Override
	public double arie() {
    	double sp = lat1+lat2+lat3;
	return Math.sqrt(sp*(sp-lat1)*(sp-lat2)*(sp-lat3));
	}

}
